package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
	"strings"
	"time"
)

// Format; Concatenate; Execute
func Transfer(f Files) (output string, err error) {

	// Local Variables
	rsync, threads := Environment()
	var paths []string
	var inode, dest_inode, echo, out string

	// Build execution commands
	for i := 0; i < len(f.Files); i++ {
		if f.Files[i].Port == "" {
			f.Files[i].Port = "22"
		}
		r := 0
		for {
			conn, err := net.DialTimeout("tcp", net.JoinHostPort(f.Files[i].Dest, f.Files[i].Port), 5*time.Second)
			r++
			if err != nil {
				fmt.Println("Connecting error, destination not available", err, "\nRetrying...\n ")
				time.Sleep(time.Second)
				if r >= 3 {
					fmt.Println("Connection Failed\n ")
					return "tcp", err
				}
			} else {
				defer conn.Close()
				fmt.Println("Connecting", net.JoinHostPort(f.Files[i].Dest, f.Files[i].Port))
				break
			}

		}
		inode = f.Files[i].Path
		if strings.HasSuffix(inode, "*") {
			inode = strings.TrimSuffix(inode, "*")
			inode = strings.TrimSuffix(inode, "/")
		}
		if f.Files[i].Targ != "" {
			dest_inode = f.Files[i].Targ
		} else {
			dest_inode = strings.Replace(inode, "~/", "", -1)
		}
		if Dir(inode) == true {
			inode = inode + "/"
		}
		if strings.HasPrefix(inode, "~") == true {
			inode = strings.Replace(inode, "~/", "", -1)
			inode = os.Getenv("HOME") + "/" + inode
		}
		paths = append(paths, fmt.Sprint(
			"\"",
			rsync+" ",
			options+" ",
			compression+" ",
			protocol+" ",
			f.Files[i].Port+"'"+" ",
			inode+" ",
			f.Files[i].Dest+":"+dest_inode+"\""))

		// Display job
		fmt.Println(" Path: "+f.Files[i].Path+"\n",
			"Inode Type: "+f.Files[i].Type+"\n",
			"Destination: "+dest_inode+"\n\n")
	}

	// Create OS processes
	inode = echo + threads + " " + strings.Join(paths, " ")
	fmt.Println("Transfering....\n ")

	// Execute
	cmd := exec.Command("/bin/sh", "-c", inode)
	stdout, err := cmd.StdoutPipe()
	cmd.Start()

	// Real time stdout
	scanner := bufio.NewScanner(stdout)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		m := scanner.Text()
		fmt.Println(m)
		out += fmt.Sprintln(m)
	}

	// Cleanup
	cmd.Wait()
	if out != "" {
		if echo != "echo " {
			fmt.Println("\nFile Transfer(s) Successful")
		}
	} else {
		log.Fatal("Invalid JSON instructions or formatting")
	}

	// Return status to HTTP
	return
}

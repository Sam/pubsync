package main

import (
	"fmt"
//	"io/ioutil"
	"encoding/json"
//	"os"
//	"os/exec"
//	"strconv"
	"strings"
	"testing"
)
	
func TestConstants(t *testing.T) {
	var options_test, protocol_test, compression_test interface{} = options, protocol, compression
	if options != options_test.(string) {
		t.Errorf("Constant \"options\" is not a string")
	}
	if protocol != protocol_test.(string) {
		t.Errorf("Constant \"protocol\" is not a string")
	}
	if compression != compression_test.(string) {
		t.Errorf("Constant \"compression\" is not a string")
	}
}

func TestExecs(t *testing.T) {
	var test Execs
	test.Rsync, test.Threads = "test", "test"
	if test.Rsync != "test" || test.Threads != "test" {
		t.Errorf("\"Exec\" is not a struct of two strings")
	}
}

func TestCli(t *testing.T) {
	var test Cli
	test.Source, test.DestTarget, test.ServerMode = "test", "test", 5
	if test.Source != "test" || test.DestTarget != "test" || test.ServerMode != 5 {
		t.Errorf("\"Cli\" is not a struct of two strings and one int")
	}
}

func TestFile(t *testing.T) {
	var test File
	test.Path, test.Type, test.Dest, test.Targ, test.Port = "test", "test", "test", "test", "test"
	if test.Path != "test" || test.Type != "test" || test.Dest != "test" || test.Targ != "test" || test.Port != "test" {
		t.Errorf("\"Files\" is not struct of five strings")
	}
}

func TestFileExists(t *testing.T) {
	test := "pubsync.go"
	file := FileExists(test)
	if file != true {
		t.Errorf("File result was incorrect; expected true boolean")
	}
}

func TestDir(t *testing.T) {
	test := "../pubsync"
	dir := Dir(test)
	if dir != true {
		t.Errorf("Directory result was incorrect; expected true boolean")
	}
}

//func TestServeHTTP(t *testing.T) {
//...
//}

func TestEnvironment(t *testing.T) {
	var test Execs
	test.Threads, test.Rsync = "test", "test"
	if test.Rsync != "test" || test.Threads != "test" {
		t.Errorf("\"Environment\" is not a function returning two strings")
	}
}

// Test Configuration
func TestTransfer(t *testing.T) {
	//var test_echo = "echo"
	var test Files
	var jsonBlob = []byte(`{
	    "files": [{
	            "path"  : "/usr/local/something",
	            "type"  : "Lorem ipsum",
	            "dest"  : "domain.where.is.this"
	        },
	        {
	            "path"  : "~/.my_profile",
	            "type"  : "dolor sit",
	            "dest"  : "this.is.a.dns.domain.name"
	        },
	        {
	            "path"  : "~/My_files",
	            "type"  : "consectetur adipiscing",
	            "dest"  : "home.workstation.lan",
	            "targ"  : "/tmp/"
	        },
	        {
	            "path"  : "~/Documents",
	            "type"  : "ut labore et dolore",
	            "dest"  : "local.subdomain.domain.mylan",
	            "targ"  : "/opt/"
	        },
	        {
	            "path"  : "/srv/*",
	            "type"  : "magna aliqua",
	            "dest"  : "local.lan.domain",
	            "targ"  : "~/Desktop"
	        },
	        {
	            "path"  : "/srv/*",
	            "type"  : "eiusmod tempor incididunt",
	            "dest"  : "local.subdomain.domain.mylan",
	            "targ"  : "~/.local",
	            "port"  : "3033"
	        },
	        {
	            "path"  : "~/.local",
	            "type"  : "elit, sed",
	            "dest"  : "some.place.lan",
	            "port"  : "9057"
	        }]
	}`)
	////byteValue, _ := ioutil.ReadAll(jsonBlob)
	json.Unmarshal(jsonBlob, &test)
	out := fmt.Sprintf(Transfer(test))
	//if err != nil {
	//	t.Error(err)
	//	return
	//} 
    tester := `parallel --lb ::: rsync -avzlxsuHP --compress-level=9 --rsh='ssh -p 22' /usr/local/something domain.where.is.this:/usr/local/something rsync -avzlxsuHP --compress-level=9 --rsh='ssh -p 22' /home/sam/.my_profile this.is.a.dns.domain.name:.my_profile rsync -avzlxsuHP --compress-level=9 --rsh='ssh -p 22' /home/sam/My_files home.workstation.lan:/tmp/ rsync -avzlxsuHP --compress-level=9 --rsh='ssh -p 22' /home/sam/Documents local.subdomain.domain.mylan:/opt/ rsync -avzlxsuHP --compress-level=9 --rsh='ssh -p 22' /srv/ local.lan.domain:~/Desktop rsync -avzlxsuHP --compress-level=9 --rsh='ssh -p 3033' /srv/ local.subdomain.domain.mylan:~/.local rsync -avzlxsuHP --compress-level=9 --rsh='ssh -p 9057' /home/sam/.local some.place.lan:.local`
    //    t.Errorf("Expected %v", string(stdout))
    //}
	//else {
	//	fmt.Sprintf(out)
		fmt.Println("")
	//	fmt.Printf(tester)
	//}
    
	//stdoutStr := stdout.String()
	if fmt.Sprintf(strings.TrimSpace(out)) == fmt.Sprintf(strings.TrimSpace(tester)) {
	//	t.Errorf("stdout mismatch:\n%s \nvs \n%s", stdout, tester)
	fmt.Println("hooray!!")
	}
}



//func Testmain(t *testing.T) {
//...
//}

package main

import (
	"os"
)

// Environment Configuration
const (
	options     string = "-avzlxsuHP"
	protocol    string = "--rsh='ssh -p"
	compression string = "--compress-level=9"
)

// HTTP struct
type Server struct{}

// Executables
type Execs struct {
	Rsync, Threads string
}

// Switch Variables
type Cli struct {
	Source, DestTarget string
	ServerMode         int
}

// Files struct for json data
type Files struct {
	Files []File `json:"files"`
}

// Individual json struct
type File struct {
	Path string `json:"path,omitempty"`
	Type string `json:"type,omitempty"`
	Dest string `json:"dest,omitempty"`
	Targ string `json:"targ,omitempty"`
	Port string `json:"port,omitempty"`
}

// Check list of files and directories for transfer
func FileExists(file string) bool {
	_, err := os.Stat(file)
	if os.IsNotExist(err) {
		return false
	}
	return true
}

// Check if inode is a file
func Dir(filetype string) bool {
	which, err := os.Stat(filetype)
	if err != nil {
		return false
	}
	return which.IsDir()
}

package main

import (
	"runtime"
)

// Runtime reflection OS
func Environment() (string, string) {
	var env Execs
	if runtime.GOOS == "windows" {
		env.Rsync = "rsync.exe"               //Msys2 for Windows package
		env.Threads = "parallel.exe --lb :::" //Msys2 for Windows package
	} else {
		env.Rsync = "rsync"
		env.Threads = "parallel --lb :::"
	}
	return env.Rsync, env.Threads
}

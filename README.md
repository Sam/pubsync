# Pubsync

--------------------------------------------------------------------------

An API driven [Rsync](https://rsync.samba.org/) manager that can broadcast 
file transfers from a central server to several individual client machines. 
The files and destinations are controlled by a JSON description via file or 
through an HTTP server interface

--------------------------------------------------------------------------

### Why?

An associate requested it, and it seemed like a good project to create with Go. If you find a valid use for it, all the better

### Why are there no goroutines?

Goroutines were impractical for simple string concatenation, and don't map directly to OS process threads.
Using Goroutines badly is much worse than not using them at all. GNU Parallel is a better way to get this specific 
task done.

### Requirements

+ rsync (both server and client machines need rsync and ssh)
+ GNU parallel (server only)
+ ssh key available in ssh-agent
+ ssh pub key available on all target servers

### Installation

+ cd pubsync && go build .
+ cp pubsync /usr/local/bin/
+ chmod u+x /usr/local/bin/pubsync

### JSON directives

The source path, and network destination are required. The destination target path will be inferred
from the source path or can be explicitly declared. You can also, optionally, use an alternate
port number for the SSH connection. The "-o" switch will override all network destinations with whatever 
is specified at the command line. 

### Usage

1. Edit the included **example.json** file to your liking
2. Run
```
$ pubsync -f example.json
```

OR

2. If you'd like to test the REST interface:
``` 
pubsync -r <port number>
curl -X POST -H "Content-Type: application/json" -d @example.json http://localhost:<port number>/   
``` 

OR

3. Send your own POST request remotely

### Caveats

You can only transfer files with appropriate account permissions; just like scp, sftp, or any similar posix utility over ssh

package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)


// HTTP server
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// Mapper for recieved JSON, capture logging for return message
	var reciever Files

	// Set to JSON
	w.Header().Set("Content-Type", "application/json")

	// Define HTTP calls
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "POST valid JSON"}`))
	case "POST":

		// Process incoming JSON
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "post called"}`))
		if r.Body == nil {
			http.Error(w, "Please send a request body", 400)
			return
		}
		err := json.NewDecoder(r.Body).Decode(&reciever)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}
		io.WriteString(w, "\n")
		io.WriteString(w, "Transfering.....\n")
		send, _ := Transfer(reciever)
		if send != "" {
			w.Write([]byte(`{"message": "File Transfer(s) Successful"}`))
			io.WriteString(w, "\n")
		} else {
			w.Write([]byte(`{"message": "File Destination Failure"}`))
			io.WriteString(w, "\n")
		}

		// Return execution status information
		io.WriteString(w, send)
		send = ""
		fmt.Println("\nCtrl+C to exit\n ")

	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "POST valid JSON"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "POST valid JSON"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "not found"}`))
	}
}

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"reflect"
	"strconv"
)

func main() {

	// Flag variables
	var cli Cli

	// Command line flag arguments
	flag.StringVar(&cli.Source, "f", "", "JSON text file of files/directories to be transfered \n")
	flag.StringVar(&cli.DestTarget, "o", "", "override all destination address(es), FQDN or IP \n")
	flag.IntVar(&cli.ServerMode, "r", 0, "run as HTTP REST API (include a valid port number)")
	flag.Parse()

	// No arguments given, and no input file, then print crib:
	if len(os.Args) < 2 && !(FileExists(cli.Source)) {
		fmt.Println("Usage: -h --help for details \n" +
			"-f: file \n" + // Source
			"-o: override destination \n" + // DestTarget
			"-r: API mode\n \n" + // Server
			"(make sure ssh key is added to ssh-agent)") // ServerMode
		os.Exit(1)

	} else if reflect.ValueOf(cli.ServerMode).IsZero() {

		// Initialize files struct
		var files Files

		// Open jsonFile and map to struct
		jsonFile, err := os.Open(cli.Source)
		if err != nil {
			log.Fatal("Error: could not read file", err)
			os.Exit(1)
		}
		fmt.Printf("Successfully Opened %v"+"\n \n", cli.Source)
		defer jsonFile.Close()
		byteValue, _ := ioutil.ReadAll(jsonFile)
		json.Unmarshal(byteValue, &files)
		if cli.DestTarget != "" {
			for i := 0; i < len(files.Files); i++ {
				files.Files[i].Dest = cli.DestTarget
			}
		}

		// Initiate transfer
		Transfer(files)
		os.Exit(0)

	} else {

		// Initiate HTTP REST interface
		fmt.Println("Starting HTTP server")
		s := &Server{}
		http.Handle("/", s)
		fmt.Printf("HTTP active on port %d \n", cli.ServerMode)
		fmt.Println("Ctrl+C to exit")
		log.Fatal(http.ListenAndServe(":"+strconv.Itoa(cli.ServerMode), nil))
	}
}
